package com.allu.quarkus;

import com.allu.quarkus.services.GreetingService;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

@QuarkusTest
public class GreetingServiceTest {
    @Inject
    GreetingService service;

    @Test
    public void testServiceName(){
        Assertions.assertEquals(service.getSeriviceName(), "Greetings");
    }
}
