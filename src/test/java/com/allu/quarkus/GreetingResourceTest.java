package com.allu.quarkus;

import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class GreetingResourceTest {


    @ConfigProperty(name="greeting.prefix")
    String prefix;


    @ConfigProperty(name="greeting.suffix")
    String suffix;

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/test")
          .then()
             .statusCode(200)
             .body(is(prefix+" Sai ram "+suffix));
    }

    @Test
    public void testNameEndpoint() {
        given()
                .when().get("/test/name/allu")
                .then()
                .statusCode(200)
                .body(is("Hello allu ,your ID is 1234"));
    }
}