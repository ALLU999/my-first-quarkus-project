package com.allu.quarkus.service;

import com.allu.quarkus.services.GreetingService;
import io.quarkus.test.Mock;

import javax.enterprise.context.ApplicationScoped;
import java.util.UUID;

@Mock
@ApplicationScoped
public class MockGreetingService extends GreetingService {

    @Override
    public String sayHello(String name) {
        return String.format("Hello %s ,your ID is %s", name ,"1234" );
    }
}
