package com.allu.quarkus.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import java.util.UUID;

@ApplicationScoped
public class GreetingService {

    private static final Logger LOGGER =  LoggerFactory.getLogger(GreetingService.class);

    public String getSeriviceName(){
        return "Greetings";
    }

    public String sayHello(String name){
        LOGGER.info("Inside Service sayHello :  "+name);

        return String.format("Hello %s ,your ID is %s", name , UUID.randomUUID().toString());
    }
}
