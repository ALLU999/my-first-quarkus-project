package com.allu.quarkus;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;

import javax.ws.rs.core.Application;

@OpenAPIDefinition(
        info=@Info(
                title = "First Project API's",
                description = "all the sample api's",
                version = "1.2"
        )
)
public class QuarkusApplication extends Application {

}
