package com.allu.quarkus;

import com.allu.quarkus.config.GreetingConfig;
import com.allu.quarkus.services.GreetingService;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

@Path("/test")
public class GreetingResource {

    private static final String Token="OM";

    @Inject
    private GreetingService service;

    @Inject
    private GreetingConfig greetingConfig;

    /*
    @ConfigProperty(name="greeting.prefix" , defaultValue = "OM")
    String prefix;

    @ConfigProperty(name="greeting.suffix" )
    Optional <String> suffix;
 */


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return greetingConfig.getPrefix()+" Sai ram "+greetingConfig.getSuffix().orElse("");
    }

    @GET
    @Path("/name/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(@PathParam("id") String name) {
        return service.sayHello(name);
    }

    @POST
    @Path("/addData")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String addData(String Data) {
        return Data;
    }

    @GET
    @Path("/isAuthorized/")
    @Produces(MediaType.TEXT_PLAIN)
    public String isAuthorized(@HeaderParam("token") String htoken, @QueryParam("token") String qtoken) {
        String token = htoken!=null ? htoken : qtoken;

        if(!Token.equals(token))
            throw new RuntimeException();
        return token;
    }

}
