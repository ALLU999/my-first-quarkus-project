package com.allu.quarkus.config;

import io.quarkus.arc.config.ConfigProperties;

import java.util.Optional;

@ConfigProperties(prefix = "greeting")
public class GreetingConfig {

    String prefix;
    Optional<String> suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Optional<String> getSuffix() {
        return suffix;
    }

    public void setSuffix(Optional<String> suffix) {
        this.suffix = suffix;
    }

}
