package com.allu.quarkus;

import com.allu.quarkus.data.Book;
import org.jboss.resteasy.annotations.Body;

import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;

@Path("/book")
public class BookResource {

    private static ArrayList<Book> books = new ArrayList<Book>();

    static {
        books.add(new Book((long) 1,"PARAYANAM","ALLU"));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBooks() {
        return Response.status(202)
                .entity(books)
                .build();
      //  return Response.ok(books).build();
        // return Response.serverError().build();
        //return Response.ok("no books").build();
       // return books;
    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addBook(@Valid Book book) {
        if(books.size() >4)
            return  Response.status(400)
                    .entity("Max 4 books are allowed")
                    .build();
        books.add(book);
        return Response.ok(book).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response updateBook(@PathParam("id") Integer index, Book book) {
        if(index >=books.size())
            return  Response.status(400)
                    .entity("index does not exist")
                    .build();
        books.remove((int) index);
        books.add(index, book);
        return Response.ok(book).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteBook(@PathParam("id") Integer index) {
        if(index >=books.size())
            return  Response.status(400)
                    .entity("index does not exist")
                    .build();
        return Response.ok(books.remove((int) index)).build();
    }
}
