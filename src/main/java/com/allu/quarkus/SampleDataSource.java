package com.allu.quarkus;

import com.mysql.cj.protocol.Resultset;
import io.agroal.api.AgroalDataSource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.transform.Result;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Path("/test-datasource")
public class SampleDataSource {

    @Inject
    AgroalDataSource dataSource;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String testSample() throws SQLException {
        CallableStatement cal=dataSource.getConnection().prepareCall("select * from sample");
        cal.execute();
        ResultSet rs=  cal.getResultSet();
        rs.next();
        return   rs.getString(2);
    }

}
